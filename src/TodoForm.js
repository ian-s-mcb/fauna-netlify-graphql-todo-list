import { useState } from 'react'

function TodoForm ({ getTodos }) {
  const [inputText, setInputText] = useState('')
  const [loading, setLoading] = useState(false)

  async function handleSubmit (e) {
    e.preventDefault()
    if (!inputText) return

    try {
      setLoading(true)
      await fetch('/.netlify/functions/createTodo', {
        method: 'POST',
        body: JSON.stringify({
          text: inputText
        })
      })
      setInputText('')
      getTodos()
    } catch (err) {
      console.log(err)
    } finally {
      setLoading(false)
    }
  }

  return (
    <form id='todoForm' onSubmit={handleSubmit}>
      <div className='input-group'>
        <input
          className='form-control'
          id='todoInput'
          onChange={e => setInputText(e.target.value)}
          placeholder='Enter a todo'
          type='text'
          value={inputText}
        />
        <button className='btn btn-primary ms-1' type='submit'>
          {loading
            ? (
              <div className='spinner-border' role='status'>
                <span className='visually-hidden'>Loading...</span>
              </div>
              )
            : 'Create'}
        </button>
      </div>
    </form>
  )
}
export default TodoForm

import { useRef, useState } from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import {
  faCheck, faRotateLeft, faTrash
} from '@fortawesome/free-solid-svg-icons'

function Todo ({ getTodos, task }) {
  const checkboxElement = useRef(null)
  const [completed, setCompleted] = useState(task.completed)
  const [deleteLoading, setDeleteLoading] = useState(false)
  const [textToBeSet, setTextToBeSet] = useState(task.text)
  const [updateLoading, setUpdateLoading] = useState(false)

  // Functions
  function handleCheck (e) {
    setCompleted(e.target.checked)
    performUpdateQuery()
  }
  async function handleDelete () {
    try {
      setDeleteLoading(true)
      await fetch('/.netlify/functions/deleteTodo', {
        method: 'POST',
        body: JSON.stringify({
          id: task._id
        })
      })
      getTodos()
    } catch (err) {
      console.error(err)
    } finally {
      setDeleteLoading(false)
    }
  }
  function handleKeyDown (e) {
    if (e.code === 'Enter') { handleUpdate() }
  }
  function handleUpdate () {
    if (!textToBeSet || (textToBeSet === task.text)) {
      revert()
    } else {
      performUpdateQuery()
    }
  }
  async function performUpdateQuery () {
    try {
      setUpdateLoading(true)
      await fetch('/.netlify/functions/updateTodo', {
        method: 'POST',
        body: JSON.stringify({
          completed: checkboxElement.current.checked,
          id: task._id,
          text: textToBeSet
        })
      })
      getTodos()
    } catch (err) {
      revert()
      console.error(err)
    } finally {
      // Reset state booleans
      setUpdateLoading(false)
    }
  }
  const revert = () => setTextToBeSet(task.text)

  // Sub-components
  const editButtons = (
    <>
      <button
        className='btn btn-outline-primary ms-2'
        onClick={handleUpdate}
      >
        {updateLoading
          ? (
            <div className='spinner-border' role='status'>
              <span className='visually-hidden'>Loading...</span>
            </div>
            )
          : <FontAwesomeIcon icon={faCheck} />}
      </button>
      <button
        className='btn btn-outline-primary ms-2'
        onClick={revert}
      >
        <FontAwesomeIcon icon={faRotateLeft} />
      </button>
    </>
  )
  const deleteButton = (
    <button
      className='btn btn-outline-danger ms-2'
      onClick={handleDelete}
    >
      {deleteLoading
        ? (
          <div className='spinner-border' role='status'>
            <span className='visually-hidden'>Loading...</span>
          </div>
          )
        : <FontAwesomeIcon icon={faTrash} />}
    </button>
  )

  return (
    <li className='list-group-item' onKeyDown={handleKeyDown}>
      <div className='d-flex align-items-center'>
        <input
          checked={completed}
          className='form-check-input m-0'
          onChange={handleCheck}
          ref={checkboxElement}
          type='checkbox'
        />
        <input
          className='form-control ms-2'
          onChange={e => setTextToBeSet(e.target.value)}
          type='text'
          value={textToBeSet}
        />
        {textToBeSet !== task.text && editButtons}
        {deleteButton}
      </div>
    </li>
  )
}

export default Todo

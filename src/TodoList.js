import { useEffect, useState } from 'react'

import Todo from './Todo'
import TodoForm from './TodoForm'

const PROFILE_LINK = 'https://iansmcb.ml/'
const LICENSE_LINK = 'https://gitlab.com/ian-s-mcb/todooly/-/blob/main/LICENSE'

function TodoList () {
  const [todos, setTodos] = useState([])

  // Functions
  async function getTodos () {
    try {
      const res = await fetch('/.netlify/functions/getTodos')
      const todos = await res.json()
      setTodos([...todos])
    } catch (err) {
      console.error(err)
    }
  }

  // Get todos on render
  useEffect(() => {
    getTodos()
  }, [])

  return (
    <>
      <h1 className='text-center display-3'>Todooly</h1>
      <TodoForm getTodos={getTodos} />
      <hr />
      <ul className='list-group'>
        {todos.map(task => (
          <Todo
            getTodos={getTodos} key={task._id} task={task}
          />))}
      </ul>
      {!todos.length && (
        <p>
          No todos to see here yet. Why not create one?
        </p>
      )}
      <footer className='mt-3 text-muted'>
        Copyright @ 2021{' '}
        <a className='text-decoration-none' href={PROFILE_LINK} >
          Ian S. McBride
        </a>.
        This work is licensed under the{' '}
        <a href={LICENSE_LINK}>MIT License</a>.
      </footer>
    </>
  )
}

export default TodoList

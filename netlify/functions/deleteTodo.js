const { DELETE_TODO } = require('./utils/todoQueries')
const sendQuery = require('./utils/sendQuery')

exports.handler = async event => {
  const { id } = JSON.parse(event.body)
  const variables = { id }

  try {
    const { deleteTodo: deletedTodo } = await sendQuery(DELETE_TODO, variables)

    return {
      statusCode: 200,
      body: JSON.stringify(deletedTodo)
    }
  } catch (err) {
    console.error(err)
    return {
      statusCode: 500,
      body: JSON.stringify({ err: 'Something went wrong' })
    }
  }
}

const GET_TODOS = `
  query {
    allTodos {
      data {
        _id
        text
        completed
      }
    }
  }
`

const CREATE_TODO = `
  mutation ($text:String!) {
    createTodo (data: {text: $text, completed: false}) {
      _id
      text
    }
  }
`

const UPDATE_TODO = `
  mutation ($id: ID!, $text:String!, $completed:Boolean!) {
    updateTodo (id: $id, data: { text: $text, completed: $completed }) {
      _id
      text
      completed
    }
  }
`

const DELETE_TODO = `
  mutation ($id: ID!) {
      deleteTodo (id: $id) {
          _id
      }
  }
`

module.exports = {
  GET_TODOS,
  CREATE_TODO,
  UPDATE_TODO,
  DELETE_TODO
}

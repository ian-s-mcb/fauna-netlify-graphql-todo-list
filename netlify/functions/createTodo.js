const { CREATE_TODO } = require('./utils/todoQueries')
const sendQuery = require('./utils/sendQuery')

exports.handler = async event => {
  const { text } = JSON.parse(event.body)
  const variables = { text }

  try {
    const { createTodo: createdTodo } = await sendQuery(CREATE_TODO, variables)

    return {
      statusCode: 200,
      body: JSON.stringify(createdTodo)
    }
  } catch (err) {
    console.error(err)
    return {
      statusCode: 500,
      body: JSON.stringify({ err: 'Something went wrong' })
    }
  }
}
